import { createSignal } from "solid-js";

const PROMPT = `Welcome, Adventure GPT. Your mission is to weave engaging, dynamic, text-based adventures for our users. You're tasked with interpreting user prompts, guiding narratives, introducing unexpected twists, and presenting challenging decisions to make every adventure unique. Your purpose is to create compelling interactive experiences that immerse users in a world of their own making. Let's begin the journey!`

const initialGameSave = (length = 32) => {
  const save = new Array(length).fill('', 0, 32)
  
  return save.map(() =>
    String.fromCharCode(parseInt((Math.random() * 94 + 33).toFixed(0)))
  ).join("")
}

export default function Home() {
  const [gameStart, setGameStart] = createSignal(false);
  const [gameSave, setGameSave] = createSignal(initialGameSave());
  const [gameInput, setGameInput] = createSignal("");
  const [gameLog, setGameLog] = createSignal([
    { 
      role: 'system', 
      content: PROMPT
    }
  ]);

  const handleCommand = () => {
    setGameLog(oldLog => [...oldLog, { role: 'user', content: gameInput() }]);
    setGameInput("");
    fetch('/game', {
      method: 'post',
      body: JSON.stringify({
        saveId: gameSave(),
        messages: gameLog(),
      })
    })
    .then(async response => await response.json())
    .then((body) => {
      // console.log(body)
      const {choices = [], usage = {}} = body
      setGameLog(oldLog => {
        const newLog = [...oldLog];
        choices.forEach(({message} : {message: {role: string, content: string}}) => {
          const {role, content} = message;
          newLog.push({role, content});
        });
        return newLog;
      });
    })
  };


  const startGame = () => {
    setGameStart(true)
    // fetch(`/game?saveId${gameSave()}`)
    //   .then(response => response.json())
    //   .then(({messages}) => {
    //     if(messages)
    //       setGameLog(messages)
    //   })
  }

  return (
    <main class="text-center mx-auto text-gray-700 p-4">
      <h1 class="max-6-xs text-6xl text-sky-700 font-thin uppercase my-16">
        Adventure GPT
      </h1>

      {gameStart() ? (
        <div>
          <textarea
            readOnly
            class="w-full h-64 bg-gray-100 p-2 mt-4 rounded"
            value={gameLog().slice(1).map(({content, role}) => `${role === 'user' ? '<' : '>'} ${content}`).join('\n')}
          />

          <textarea
            class="w-full mt-4 p-2 rounded"
            placeholder={gameLog().slice(1).length === 0 ? 'Enter Starting Scenario' : "Type your command here..."}
            value={gameInput()}
            onInput={(e) => setGameInput(e.currentTarget.value)}
          />

          <button
            onClick={handleCommand}
            class="w-full mt-4 p-2 bg-sky-600 text-white rounded"
          >
            {gameLog().slice(1).length === 0 ? 'Start' : 'Enter Command'}
          </button>
        </div>
      ) : (
        <div>
          <p class="mt-8 text-lg text-gray-500">
            To start the game, click the start button. You'll see the game's
            narrative in a box above, and you can type your commands in the
            box below.
          </p>

          <input
            class="w-full mt-4 p-2 rounded"
            value={gameSave()}
            onInput={(e) => setGameSave(e.currentTarget.value)}
          />

          <button
            onClick={() => startGame()}
            class="mt-4 p-2 bg-sky-600 text-white rounded"
          >
            Start Game
          </button>
        </div>
      )}
    </main>
  );
}

