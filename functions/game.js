export async function onRequest({request, env}) {
  if(request.method.toLowerCase() === 'post') {
    const {saveId, messages} = await request.json()
    await env.GAMES.put(`games:${saveId}`, JSON.stringify(messages))
    let { readable, writable } = new TransformStream();
    const response = env.OPENAI.fetch('https://openai/completion', {
      method: 'post',
      body: JSON.stringify({
        model: 'gpt-4',
        messages,
        temperature: 0.7,
        maxTokens: 2000
      })
    })
    // ... and deliver our Response while that’s running

    return response
  }

  const saveId = new URL(request.url).searchParams.get("saveId")
  return new Response(await env.GAMES.get(`games:${saveId}`))
}